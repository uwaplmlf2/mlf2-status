function pad(n) {
    return n<10 ? '0'+n : n;
}

exports.isoformat = function(d) {
    return d.getUTCFullYear()+'-'
	+ pad(d.getUTCMonth()+1)+'-'
	+ pad(d.getUTCDate())+'T'
	+ pad(d.getUTCHours())+':'
	+ pad(d.getUTCMinutes())+':'
	+ pad(d.getUTCSeconds())+'+00:00';
}
exports.iso8601 = exports.isoformat;

function rfc3339(date) {
  return date.getUTCFullYear()   + '-' +
    pad(date.getUTCMonth() + 1) + '-' +
    pad(date.getUTCDate())      + 'T' +
    pad(date.getUTCHours())     + ':' +
    pad(date.getUTCMinutes())   + ':' +
    pad(date.getUTCSeconds())   + 'Z';
};
exports.rfc3339 = rfc3339;

function parseISO8601(s) {
    var tz = s.slice(-6).replace(/:/, '');
    var dt = s.slice(0, -6);

    return new Date(dt.replace(/-/g, '/').replace(/T/, ' ') + ' GMT'+tz);
}
exports.parseISO8601 = parseISO8601;

function endOfDay(d) {
    var eod = new Date(d.getTime());
    eod.setUTCHours(23, 59, 59);
    return eod;
}
exports.endOfDay = endOfDay;

exports.uniq = function(arr) {
    var dict = {};
    if (arr) {
        for(var i = 0;i < arr.length;i++) {
	    dict[arr[i]] = 1;
        }
    }

    var keys = [];
    for(var prop in dict) {
	keys.push(prop);
    }

    return keys;
}


// Class to represent an angle as degrees and decimal minutes
var Dmm = function(deg) {
    if(deg == null) {
	this.deg = 0;
	this.min = 0.;
    } else {
	if(deg < 0)
	    this.deg = Math.ceil(deg);
	else
	    this.deg = Math.floor(deg);

	this.min = (deg - this.deg)*60.;
    }
}

Dmm.prototype = {
    toString: function() {
	var deg = Number(this.deg);
	var min = Number(Math.abs(this.min));
	var padding = (min < 10.) ? "0" : "";
	return deg.toFixed() + "-" + padding + min.toFixed(4);
    },
    toDegrees: function() {
	return this.deg + this.min/60.;
    },
    isneg: function() {
	return ((this.deg < 0) ? true : false);
    },
    abs: function() {
	var d = new Dmm(0);
	d.deg = Math.abs(this.deg);
	d.min = this.min;
	return d;
    }
}

var Decimal = function(n, prec) {
    this.value = Number(n);
    this.precision = prec == undefined ? -1 : prec;
}

Decimal.prototype = {
    toString: function() {
	if(this.precision >= 0)
	    return this.value.toFixed(this.precision);
	else
	    return this.value.toString();
    }
}

exports.format_status = function(status) {
    var lat = new Dmm(status.gps.lat);
    var lon = new Dmm(status.gps.lon);
    var t = parseISO8601(status.timestamp);
    var alerts = '';

    var hlat = "N", hlon = "E";

    if(lat.isneg()) {
	lat = lat.abs();
	hlat = "S";
    }

    if(lon.isneg()) {
	lon = lon.abs();
	hlon = "W";
    }

    if(status.alerts) {
	alerts = status.alerts.join('\n');
    }

    return 'float-' + status.floatid + ' '
          + lat.toString() + hlat + ' '
          + lon.toString() + hlon + ' '
          + status.gps.nsat + ' '
          + rfc3339(t) + '\n' + alerts;
}

if(exports) {
    exports.Dmm = Dmm;
    exports.Decimal = Decimal;
}
