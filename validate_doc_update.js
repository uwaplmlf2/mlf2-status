function (newDoc, oldDoc, userCtx) {
    var doc_type = newDoc.type;

    function forbidden(message) {
        throw({forbidden : message});
    }

    function require(beTrue, message) {
        if (!beTrue)
            forbidden(message);
    }

    function is_admin(roles) {
        return (roles.indexOf("_admin") != -1 || roles.indexOf("mlf2_admin") != -1);
    }

    if(newDoc._deleted) {
        require(is_admin(userCtx.roles),
                "Only admin users can delete a document");
        return;
    }

    if(doc_type == "active") {
        var re = /^apl\.uw\.mlf2:(\d+)/;

        if(!oldDoc)
            require(is_admin(userCtx.roles), "Only admin users can add a new float");

        require(newDoc.floatid != undefined, "Float ID must be specified");

        var result = newDoc._id.match(re);
        require(result && parseInt(result[1]) == newDoc.floatid,
            "Invalid document ID");
        require(newDoc.imei != undefined, "Float Iridium IMEI must be specified");
    }

    if(doc_type == "inactive") {
        require(is_admin(userCtx.roles), "Only admin users can remove a float");
        require(newDoc.floatid != undefined, "Float ID must be specified");
    }

    if(doc_type == "status") {
        require(userCtx.roles.indexOf("data_source") != -1, "You are not permitted to add/change data");
        require(newDoc.floatid != undefined, "Float ID must be specified");
        require(newDoc.timestamp, "Timestamp must be specified");
    }

    if(doc_type == "data") {
        require(userCtx.roles.indexOf("data_source") != -1, "You are not permitted to add/change data");
        require(newDoc.floatid != undefined, "Float ID must be specified");
        require(newDoc.t_start && newDoc.t_stop, "Start/end times must be specified");
    }

    if(doc_type == "deployment") {
        var re = /^apl\.uw\.mlf2:\D[\w\-]*/;

        require(newDoc._id.match(re), "Invalid document ID");
        require(newDoc.start, "Mission start date/time must be specified");
        require(newDoc.stop, "Mission stop date/time must be specified");
        require(newDoc.name, "Mission name must be specified");
        require(typeof newDoc.floats == 'object' && newDoc.floats.length > 0,
	       "At least one float ID must be specified");
    }
}
