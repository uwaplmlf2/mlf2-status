function(doc) {
    if(doc.type == "status") {
	emit([doc.floatid, doc.timestamp], 
	     {mode: doc.mode,
	      lat: doc.gps.lat,
	      lon: doc.gps.lon,
	      ipr: doc.sensors.ipr,
	      rh: doc.sensors.rh,
              voltage: doc.sensors.voltage,
              piston_error: doc.piston_error,
	      alerts: doc.alerts});
    }
}
