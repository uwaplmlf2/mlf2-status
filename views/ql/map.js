function(doc) {
    //!code helpers/utils.js
    if(doc.type == 'data' && doc.filename.slice(0, 2) == 'ql') {
	var d = new Date(doc.t_stop*1000);
	emit([doc.floatid, isoformat(d)], doc);
    }  
}
