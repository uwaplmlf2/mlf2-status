function(doc) {
    //!code helpers/utils.js
    if(doc.type == 'data') {
	var d = new Date(doc.t_stop*1000);
	emit([doc.floatid, isoformat(d)], doc);
    }
}
