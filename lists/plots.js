function(head, req) {
    var Atom = require('lib/atom');

    var row;
    var first = true;
    var db_name = req.info.db_name;

    send('[');
    while(row = getRow()) {
	var doc = row.value;
	for(var name in doc._attachments) {
	    if(doc._attachments[name].content_type == 'image/png') {
		if(first) {
		    first = false;
		} else {
		    send(',');
		}
		var obj = {url: '/'+db_name+'/'+row.id+'/'+name,
			   d_start: Atom.date_format(new Date(doc.t_start*1000)),
			   d_stop: Atom.date_format(new Date(doc.t_stop*1000))};
		send(JSON.stringify(obj)+'\n');
	    }
	}
    }
    send(']');
}
