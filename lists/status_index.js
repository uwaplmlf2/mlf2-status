function(head, req) {
    var ddoc = this;
    var Mustache = require('lib/mustache');
    var path = require('vendor/couchapp/lib/path').init(req);
    var List = require('vendor/couchapp/lib/list');
    var Atom = require('lib/atom');
    var util = require('lib/utils');

    var indexPath = path.list('status_index', 'recent-status', {descending: true, limit: 20});
    var feedPath = path.list('status_index', 'recent-status', {descending: true, limit: 10, format: 'atom'});
    var dataFeed = path.list('file_index', 'recent-data', {descending: true, limit: 10, format: 'atom'});
    var msglink = ['', req.path[0], req.path[1], 'messaging', 'index.html'].join('/');
    var source;
    var template;

    if(req.path[5] == 'recent-status') {
	source = '';
	template = ddoc.templates.status_index;
    } else {
	source = 'Float-' + req.query.startkey[0];
	template = ddoc.templates.float_status;
    }

    provides('html', function() {
		 var key = "";
		 var data = {
		     header: {
			 source: source,
			 index: indexPath,
			 feedPath: feedPath,
			 dataFeed: dataFeed,
			 msglink: msglink
		     },
		     scripts: {},
		     ui_style: {},
		     db: req.path[0],
		     source: source,
		     design: req.path[2],
		     feedPath: feedPath,
		     indexPath: indexPath,
		     assets: path.asset(),
		     items: List.withRows(function(row) {
					      var doc = row.value;
					      key = row.key;
					      var t = util.parseISO8601(doc.timestamp || key[1]);
					      var floatid = doc.floatid;
					      var lat = new util.Dmm(doc.lat || doc.gps.lat);
					      var lon = new util.Dmm(doc.lon || doc.gps.lon);
					      var hlat = "N", hlon = "E";
    
					      if(lat.isneg()) {
						  lat = lat.abs();
						  hlat = "S";
					      }
					      
					      if(lon.isneg()) {
						  lon = lon.abs();
						  hlon = "W";
					      }

					      var url = path.show('gmap', row.id, {days: 10});
					      var floatlink = path.list('status_index', 'status',
									{descending: true,
									 limit: 20,
									 startkey: [floatid, {}],
									 endkey: [floatid]});
					      return {
						  floatid: floatid,
						  floatlink: floatlink,
						  timestamp: Atom.date_format(t),
						  state: doc.mode,
						  position: lat.toString() + hlat + '/' + lon.toString() + hlon,
						  url: url
					      };
					  }),
		     older: function () {
			 return path.older(key);
		     },
		     '10': path.limit(10),
		     '20': path.limit(20),
		     '40': path.limit(40)
		     };
		 return Mustache.to_html(template, data, ddoc.templates.partials, List.send);
	     });

    provides('atom', function() {
		 var row = getRow();
		 var updated = (row ? util.parseISO8601(row.value.timestamp || row.key[1]) : new Date());

		 var feedHeader = Atom.header({
						  updated: updated,
						  title: 'MLF2 Status',
						  feed_id: path.absolute(indexPath),
						  feed_link: path.absolute(feedPath)
					      });
		 send(feedHeader);

		 if(row) {

		     do {
			 var doc = row.value;
			 var prefix = path.absolute('/'+req.info.db_name+'/'+row.id);

			 var feedEntry = Atom.entry({
							entry_id: prefix,
							title: 'MLF2 ' + doc.floatid + ' Status Update',
							updated: util.parseISO8601(doc.timestamp || row.key[1]),
							content_type: 'text',
							content: util.format_status(doc),
							author: 'float-'+doc.floatid,
							alternate: path.absolute(path.show('gmap', row.id, {days: 10}))
						    });
			 send(feedEntry);
		     } while(row = getRow());
		 }
		 return '</feed>';
	     });
}
