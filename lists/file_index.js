function(head, req) {
    var ddoc = this;
    var Mustache = require('lib/mustache');
    var path = require('vendor/couchapp/lib/path').init(req);
    var List = require('vendor/couchapp/lib/list');
    var Atom = require('lib/atom');

    var dataIndex = path.list('file_index', 'recent-data', {descending: true, limit: 20});
    var feedPath = path.list('file_index', 'recent-data', {descending: true, limit: 10, format: 'atom'});
    var indexPath = path.list('status_index', 'recent-status', {descending: true, limit: 20});
    var statusFeed = path.list('status_index', 'recent-status', {descending: true, limit: 10, format: 'atom'});
    var msglink = ['', req.path[0], req.path[1], 'messaging', 'index.html'].join('/');
    var source;

    if(req.path[5] == 'recent-data') {
        source = 'all MLF2 floats';
    } else {
        source = 'MLF2 float-' + req.query.startkey[0];
    }

    provides('html', function() {
		 var key = "";
		 var data = {
             header: {
                 index: indexPath,
                 feedPath: statusFeed,
                 dataFeed: feedPath,
                 msglink: msglink
		     },
		     scripts: {},
		     ui_style: {},
		     source: source,
		     dataIndex: dataIndex,
		     db: req.path[0],
		     design: req.path[1],
		     feedPath: feedPath,
		     assets: path.asset(),
		     items: List.withRows(function(row) {
					      var doc = row.value;
					      var alts = [];
					      key = row.key;
					      var d_start = new Date(doc.t_start*1000);
					      var d_stop = new Date(doc.t_stop*1000);
					      var url = '/'+req.info.db_name+'/'+row.id+'/'+doc.filename;
					      for(var name in doc._attachments) {
                              var obj = doc._attachments[name];
                              if(name != doc.filename) {
                                  var fileurl = '/'+req.info.db_name+'/'+row.id+'/'+name;
                                  if(obj.content_type == 'image/png') {
                                      alts.push({text: '[PNG]',
                                                 url: fileurl,
                                                 klass: 'plot'});
                                  }
                                  if(obj.content_type == 'application/x-zip' ||
                                      obj.content_type == 'application/zip') {
                                      alts.push({text: '[ZIP]',
                                                 url: fileurl,
                                                 klass: 'data'});
                                  }
                              }
					      }
					      var floatlink = path.list('file_index', 'alldata',
									{descending: true,
									 limit: 20,
									 reduce: false,
									 startkey: [doc.floatid, {}],
									 endkey: [doc.floatid]});
					      return {
						  floatid: doc.floatid,
						  floatlink: floatlink,
						  alts: alts,
						  name: doc.filename,
						  d_start: Atom.date_format(d_start),
						  d_stop: Atom.date_format(d_stop),
						  url: url
					      };
					  }),
		     older: function () {
			 return path.older(key);
		     },
		     '10': path.limit(10),
		     '20': path.limit(20),
		     '40': path.limit(40)
		     };
		 return Mustache.to_html(ddoc.templates.file_index, data, ddoc.templates.partials, List.send);
	     });

    provides('atom', function() {
		 var row = getRow();
		 var updated = (row ? new Date(row.value.t_stop*1000) : new Date());

		 var feedHeader = Atom.header({
						  updated: updated,
						  title: 'MLF2 Data Files',
						  feed_id: path.absolute(dataIndex),
						  feed_link: path.absolute(feedPath)
					      });
		 send(feedHeader);

		 if(row) {

		     do {
			 var doc = row.value;
			 var enclosures = [];
			 var prefix = path.absolute('/'+req.info.db_name+'/'+row.id);
			 for(var name in doc._attachments) {
			     var obj = doc._attachments[name];
			     enclosures.push({
						 href: prefix+'/'+name,
						 type: obj.content_type,
						 length: obj.length
					     });
			 }

			 var feedEntry = Atom.entry({
							entry_id: prefix,
							title: 'MLF2 ' + doc.floatid + ' ' + doc.filename,
							updated: new Date(doc.t_stop*1000),
							content: 'New data file uploaded from float-' + doc.floatid,
							author: 'float-'+doc.floatid,
							enclosures: enclosures
						    });
			 send(feedEntry);
		     } while(row = getRow());
		 }
		 return '</feed>';
	     });
}
