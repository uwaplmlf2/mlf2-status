function(head, req) {
    var ddoc = this;
    var Mustache = require('lib/mustache');
    var path = require('vendor/couchapp/lib/path').init(req);
    var List = require('vendor/couchapp/lib/list');

    start({'headers': {'Content-Type': 'application/vnd.google-earth.kml+xml'}});

    var first = getRow();
    var floatid = first.key[0];
    var icon = ['', req.path[0], 'apl.uw.mlf2:'+floatid, 'marker.png'].join('/');
    var dot_icon = path.asset('images', 'dot.png');
    var colors = ['ffff55ff',
		  'ff0000aa',
		  'ff00aa00',
		  'ff0055aa',
		  'ffaa0000',
		  'ff00ffff',
		  'ffffff55',
		  'ffffffff'];
    var attributes = [];

    for(var name in first.value) {
	attributes.push({name: name, value: first.value[name]});
    }

    var params = {
	styles: {
	    linewidth: 1,
	    linecolor: colors[floatid%8],
	    iconurl: path.absolute(icon)
	},
	floatloc: {
	    timestamp: first.key[1],
	    lat: first.value.lat,
	    lon: first.value.lon,
	    floatid: floatid,
	    iconurl: path.absolute(icon),
	    attributes: attributes
	},
	floatid: floatid,
	lat: first.value.lat,
	lon: first.value.lon,
	linewidth: 1,
	linecolor: 'ffff00ff',
	points: List.withRows(function(row) {
				  return {
				      lat: row.value.lat,
				      lon: row.value.lon
				  };
			      })
	};

    
    Mustache.to_html(ddoc.templates.kml.simple_track, params,
		    ddoc.templates.partials, List.send);
}
