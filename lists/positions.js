function(head, req) {
    var ddoc = this;
    var Mustache = require('lib/mustache');
    var path = require('vendor/couchapp/lib/path').init(req);
    var List = require('vendor/couchapp/lib/list');

    start({'headers': {'Content-Type': 'application/vnd.google-earth.kml+xml'}});

    var dot_icon = path.asset('images', 'dot.png');
    var first = getRow();

    var params = {
	styles: {
	    breadcrumburl: path.absolute(dot_icon)
	},
	floatid: first.key[0],
	points: List.withRows(function(row) {
				  return {
				      timestamp: row.key[1],
				      floatid: row.key[0],
				      lat: row.value.lat,
				      lon: row.value.lon
				  };
			      })
	};

    
    Mustache.to_html(ddoc.templates.kml.positions, params,
		    ddoc.templates.partials, List.send);
}
