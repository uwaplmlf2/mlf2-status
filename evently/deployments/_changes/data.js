function (data) {
    var app = $$(this).app;
    var path = app.require('vendor/couchapp/lib/path').init(app.req);
    var missions = [];

    data.rows.forEach(function(r){
        var doc = r.value;
        var url = path.show('mission_kml', r.id);
        missions.push({url: url,
                       name: doc.name});
    });

    return {missions: missions};
}