function(data) {
    var app = $$(this).app;
    var path = app.require('vendor/couchapp/lib/path').init(app.req);
    var util = app.require('lib/utils');
    var floats = [];
    data.rows.forEach(function(r) {
			  var floatlink = path.list('status_index', 'status',
						    {descending: true,
						     limit: 20,
						     startkey: [r.doc.floatid, {}],
						     endkey: [r.doc.floatid]});
			  var last_login = "";
			  if(r.doc.last_login) {
			      var t = util.parseISO8601(r.doc.last_login);
			      last_login = util.rfc3339(t);
			  }
			  floats.push({floatid: r.doc.floatid,
				       floatlink: floatlink,
				       last_login: last_login});
		      });
    return {
	floats: floats
    };
}
