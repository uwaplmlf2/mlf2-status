function(doc, req) {  
    var ddoc = this;
    var Mustache = require('vendor/couchapp/lib/mustache');
    var path = require('vendor/couchapp/lib/path').init(req);
    var util = require('lib/utils');

    if(!doc)
        throw({forbidden: 'No document specified'});

    if(doc.type != 'deployment')
        throw({forbidden: 'Invalid document type'});

    var dstart, dstop;
    if(doc.stop) {
        dstop = doc.stop;
    } else {
        dstop = util.isoformat(new Date());
    }

    dstart = doc.start;

    var floats = [];

    doc.floats.forEach(function(elem, idx) {
			   var track_path = path.list('simple_track', 'status', 
						      {descending: true,
						       startkey: [elem, dstop],
						       endkey: [elem, dstart]});
			   var history_path = path.list('positions', 'status', 
							{descending: true,
							 startkey: [elem, dstop],
							 endkey: [elem, dstart]});
			   floats.push({floatid: elem,
				        track_url: path.absolute(track_path),
				        history_url: path.absolute(history_path)});
		       });

    var data = {
        name: doc.name,
        floats: floats
    };

    return {body: Mustache.to_html(ddoc.templates.kml.mission, data),
            headers: {
                "Content-Type": "application/vnd.google-earth.kml+xml",
                "Content-Disposition": "attachment; filename="+doc._id+".kml"}};
    
}
