function(doc, req) {  
    var ddoc = this;
    var Mustache = require('vendor/couchapp/lib/mustache');
    var path = require('vendor/couchapp/lib/path').init(req);
    var icon = ['', req.path[0], 'apl.uw.mlf2:'+doc.floatid, 'marker.png'].join('/');

    var data = {timestamp: doc.timestamp,
		lat: doc.gps.lat,
		lon: doc.gps.lon,
		floatid: doc.floatid,
		iconurl: path.absolute(icon)};
    return {body: Mustache.to_html(ddoc.templates.kml.place, data),
	    headers: {
		"Content-Type": "application/vnd.google-earth.kml+xml"}};
}
