function(doc, req) {  
    var ddoc = this;
    var Mustache = require('vendor/couchapp/lib/mustache');
    var path = require('vendor/couchapp/lib/path').init(req);
    var util = require('lib/utils');
    var icon = ['', req.path[0], 'apl.uw.mlf2:'+doc.floatid, 'marker.png'].join('/');

    var days = req.query.days ? req.query.days : 5;

    if(!doc)
	throw({forbidden: 'No document specified'});

    var dstop = util.parseISO8601(doc.timestamp);
    var dstart = new Date(dstop);
    dstart.setUTCDate(dstart.getUTCDate() - days);

    var track_path = path.list('simple_track', 'status', {descending: true,
							  startkey: [doc.floatid, util.isoformat(dstop)],
							  endkey: [doc.floatid, util.isoformat(dstart)]});
    var history_path = path.list('positions', 'status', {descending: true,
							 startkey: [doc.floatid, util.isoformat(dstop)],
							 endkey: [doc.floatid, util.isoformat(dstart)]});
    var file_path = path.list('file_index', 'alldata', {descending: true,
							reduce: false,
							limit: 20,
							startkey: [doc.floatid, util.isoformat(util.endOfDay(dstop))],
							endkey: [doc.floatid]});
    var dataIndex = path.list('file_index', 'recent-data', {descending: true, limit: 20});
    var feedPath = path.list('file_index', 'recent-data', {descending: true, limit: 10, format: 'atom'});
    var indexPath = path.list('status_index', 'recent-status', {descending: true, limit: 20});
    var statusFeed = path.list('status_index', 'recent-status', {descending: true, limit: 10, format: 'atom'});
    var msglink = ['', req.path[0], req.path[1], 'messaging', 'index.html'].join('/');

    var data = {
	header: {
	    index: indexPath,
	    feedPath: statusFeed,
	    dataFeed: feedPath,
	    source: 'Float-' + doc.floatid,
        msglink: msglink
	},
	assets: path.asset(),
	db: req.path[0],
	design: req.path[1],
	timestamp: dstop.toUTCString(),
	lat: doc.gps.lat,
	lon: doc.gps.lon,
	nsat: doc.gps.nsat,
	ipr: doc.sensors.ipr,
	rh: doc.sensors.rh,
	v1: doc.sensors.voltage[0]/1000.,
	v2: doc.sensors.voltage[1]/1000.,
	episton: doc.piston_error,
	alerts: util.uniq(doc.alerts).join('. '),
	floatid: doc.floatid,
	dfree: doc.freespace,
	dstop: util.isoformat(dstop),
	dstart: util.isoformat(dstart),
	iconurl: path.absolute(icon),
	track_url: path.absolute(track_path),
	file_url: path.absolute(file_path),
	history_url: path.absolute(history_path)};

    return {
	body: Mustache.to_html(ddoc.templates.gmap, data, ddoc.templates.partials)
    };
}
